import re
import pickle
import logging

import scrapy
from scrapy.utils.log import configure_logging
from .. import env

configure_logging(install_root_handler=False)

logging.basicConfig(
    filename='log.txt',
    format='%(levelname)s: %(message)s',
    level=logging.INFO
)


class PagesSpider(scrapy.Spider):
    name = 'pages'
    allowed_domains = [env('ALLOWED_DOMAIN'), ]

    def __init__(self, filename=None):
        super(PagesSpider, self).__init__()
        if filename:
            with open(filename, 'r') as f:
                self.start_urls = f.readlines()
        else:
            with open('start_urls.pickle', 'rb') as fp:
                self.start_urls = pickle.load(fp)

    def parse(self, response):
        meta_title = response.xpath("//meta[@name='title']/@content").extract_first()
        meta_description = response.xpath("//meta[@name='description']/@content").extract_first()
        meta_keywords = response.xpath("//meta[@name='keywords']/@content").extract_first()
        yield {
            meta_title: meta_title,
            meta_description: meta_description,
            meta_keywords: meta_keywords,
        }


